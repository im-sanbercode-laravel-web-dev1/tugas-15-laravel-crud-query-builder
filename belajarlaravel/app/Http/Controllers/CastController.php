<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// Tambahkan Query Builder
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class CastController extends Controller
{
    public function create()
    {
        return view('cast.tambah');
    }

    public function kirim(Request $request)
    {
        // Validasi Apabila Form Kosong
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        // Tambah Kirim Data
        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);

        return redirect('/cast');
    }

    public function index()
    {
        $cast = DB::table('cast')->get();

        return view('cast.tampil', ['cast' => $cast]);
    }

    public function show($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();

        return view('cast.detail', ['cast' => $cast]);
    }

    public function edit($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();

        return view('cast.edit', ['cast' => $cast]);
    }

    public function update(Request $request, $id)
    {
        // Validasi Apabila Form Kosong
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        DB::table('cast')
            ->where('id', $id)
            ->update(
                [
                    'nama' => $request->nama,
                    'umur' => $request->umur,
                    'bio'  => $request->bio
                ]
            );

        return redirect('/cast');
    }

    public function destroy($id)
    {
        DB::table('cast')->where('id', $id)->delete();

        return redirect('/cast');
    }
}
