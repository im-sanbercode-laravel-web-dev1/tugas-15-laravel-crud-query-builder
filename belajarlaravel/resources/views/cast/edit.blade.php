@extends('layout.master')

@section('judul')
Halaman Edit  Cast   
@endsection

@section('konten')

<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('PUT')
  <div class="form-group">
    <label for="formGroupExampleInput">Nama</label>
    <input type="text" class="form-control" value="{{$cast->nama}}" name="nama">
  </div>
    @error('nama')
    <div class="alert alert-danger">{{$message}}</div>
    @enderror
    
  <div class="form-group">
    <label>Umur</label>
    <input type="number" class="form-control" value="{{$cast->umur}}" name="umur">
  </div>
    @error('umur')
    <div class="alert alert-danger">{{$message}}</div>
    @enderror

  <div class="form-group">
    <label for="formGroupExampleInput2">Bio</label>
    <textarea name="bio" class="form-control" cols="20" rows="10">{{$cast->bio}}</textarea>
  </div>
    @error('bio')
    <div class="alert alert-danger">{{$message}}</div>
    @enderror

  <button type="submit" class="btn btn-primary">Simpan</button>
</form>

@endsection