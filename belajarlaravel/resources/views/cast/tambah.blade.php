@extends('layout.master')

@section('judul')
Halaman Tambah Cast   
@endsection

@section('konten')

<form action="/cast" method="POST">
    @csrf
  <div class="form-group">
    <label for="formGroupExampleInput">Nama</label>
    <input type="text" class="form-control" name="nama" id="formGroupExampleInput">
  </div>
    @error('nama')
    <div class="alert alert-danger">{{$message}}</div>
    @enderror
    
  <div class="form-group">
    <label for="formGroupExampleInput2">Umur</label>
    <input type="text" class="form-control" name="umur" id="formGroupExampleInput2">
  </div>
    @error('umur')
    <div class="alert alert-danger">{{$message}}</div>
    @enderror

  <div class="form-group">
    <label for="formGroupExampleInput2">Bio</label>
    <textarea name="bio"  class="form-control" cols="20" rows="10"></textarea>
  </div>
    @error('bio')
    <div class="alert alert-danger">{{$message}}</div>
    @enderror

  <button type="submit" class="btn btn-primary">Simpan</button>
</form>

@endsection