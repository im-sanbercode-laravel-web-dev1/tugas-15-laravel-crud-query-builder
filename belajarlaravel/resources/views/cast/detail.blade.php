@extends('layout.master')

@section('judul')
Halaman Detail Cast   
@endsection

@section('konten')

<a href="/cast" class="btn btn-primary">Kembali</a>
<br>
<br>
<h3>{{$cast->nama}}</h3>
<h5>{{$cast->bio}}</h5>

@endsection