<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'utama']);

Route::get('/biodata', [HomeController::class, 'biodata']);

Route::post('/kirim', [HomeController::class, 'kirim']);

Route::get('/table', [HomeController::class, 'table1']);

Route::get('/data-table', [HomeController::class, 'table2']);

// Route::get('/data-table', function () {
//     return view('halaman.data-table');
// });

// CRUD Cast

// Create
Route::get('/cast/create', [CastController::class, 'create']);
// Kirim Data Ke Database == Tambah Data
Route::post('/cast', [CastController::class, 'kirim']);

// Read
// Tampil Semua Data
Route::get('/cast', [CastController::class, 'index']);
// Detail Cast Berdasarkan ID
Route::get('/cast/{cast_id}', [CastController::class, 'show']);

//Update
//Form Update Kategori
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
// Update Data Ke Database Berdasarkan ID
Route::put('/cast/{cast_id}', [CastController::class, 'update']);

//Delete
//Delete Berdasarkan ID
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);
